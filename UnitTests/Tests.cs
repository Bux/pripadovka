﻿using NSubstitute;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using WatchDog.Logic;
using WatchDog.Model;
using WatchDog.SingletonCollections;
using Xunit;

namespace UnitTests
{
    public class Tests
    {

        [Fact]
        public void Trigger_5_deals_DealsAddedInOrder()
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(@"[" +
                "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000000,\"Counter\":1,\"ServerName\":\"103.40.209.25:443\",\"Login\":4001}," +
                "{\"Id\":2,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":95.1,\"Time\":1000000001000,\"Counter\":2,\"ServerName\":\"103.40.209.25:443\",\"Login\":4003}," +
                "{\"Id\":3,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":104,\"Time\":1000000002000,\"Counter\":3,\"ServerName\":\"103.40.209.25:443\",\"Login\":4004}," +
                "{\"Id\":4,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000003000,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":5,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000007000,\"Counter\":5,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}" +
                "]");

            var dict = new SingletonDictionary();


            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }

            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Select(x => x.Id).OrderBy(y => y);
            var actual = collection.Select(x => x.Id);
            Assert.True(actual.SequenceEqual(expected));
        }


        [Fact]
        public void Trigger_6_deals_DealsAddedInOrderAndRemoved()
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(@"[" +
                "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000000,\"Counter\":1,\"ServerName\":\"103.40.209.25:443\",\"Login\":4001}," +
                "{\"Id\":2,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":95.1,\"Time\":1000000001000,\"Counter\":2,\"ServerName\":\"103.40.209.25:443\",\"Login\":4003}," +
                "{\"Id\":3,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":104,\"Time\":1000000002000,\"Counter\":3,\"ServerName\":\"103.40.209.25:443\",\"Login\":4004}," +
                "{\"Id\":4,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000003000,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":5,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000004000,\"Counter\":5,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}" +
                "]");

            const int openDeltaTime = 2000;

            var dict = new SingletonDictionary();


            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
                dict.RemoveOldElements(data[i], openDeltaTime);
            }

            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Where(z => z.Id >= 3).Select(x => x.Id).OrderBy(y => y);
            var actual = collection.Select(x => x.Id);
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void Trigger_15__Deals_15_events_triggered()
        {

            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(@"[" +
                "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000010,\"Counter\":1,\"ServerName\":\"103.40.209.25:443\",\"Login\":4001}," +
                "{\"Id\":2,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":95,\"Time\":1000000000030,\"Counter\":2,\"ServerName\":\"103.40.209.25:443\",\"Login\":4003}," +
                "{\"Id\":3,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000050,\"Counter\":3,\"ServerName\":\"103.40.209.25:443\",\"Login\":4004}," +
                "{\"Id\":4,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000080,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":5,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000110,\"Counter\":5,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":6,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000130,\"Counter\":6,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":7,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000150,\"Counter\":7,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":8,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000180,\"Counter\":8,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":9,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000210,\"Counter\":9,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":10,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000230,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":11,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000250,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":12,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000280,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":13,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000310,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":14,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000330,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                "{\"Id\":15,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000000350,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}" +
                "]");

            var dict = new SingletonDictionary();

            List<string> receivedEvents = new List<string>();

            dict.DealAddedToQueueEventHandler += delegate (Deal deal)
            {
                receivedEvents.Add(deal.Id.ToString());
            };

            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }

            Assert.Equal(data.Count, receivedEvents.Count);
            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Select(x => x.Id).OrderBy(y => y);
            Assert.True(collection.Select(x => x.Id).SequenceEqual(expected));
        }

        [Fact]
        public void Trigger_5_deals_Find_5_Matches()
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(@"[" +
                                                                                 "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000000,\"Counter\":1,\"ServerName\":\"103.40.209.25:443\",\"Login\":4001}," +
                                                                                 "{\"Id\":2,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":95.1,\"Time\":1000000001000,\"Counter\":2,\"ServerName\":\"103.40.209.25:443\",\"Login\":4003}," +
                                                                                 "{\"Id\":3,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":104,\"Time\":1000000002000,\"Counter\":3,\"ServerName\":\"103.40.209.25:443\",\"Login\":4004}," +
                                                                                 "{\"Id\":4,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":103,\"Time\":1000000003000,\"Counter\":4,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}," +
                                                                                 "{\"Id\":5,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":98,\"Time\":1000000007000,\"Counter\":5,\"ServerName\":\"103.40.209.25:443\",\"Login\":4008}" +
                                                                                 "]");

            ConfigurationManager.AppSettings["openTimeDelta"] = "2000";
            ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"] = "5";

            var loggerMock = Substitute.For<ILogger>();
            var dict = new SingletonDictionary();
            var detector = new Detector(loggerMock, dict);

            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }

            loggerMock.Received(1)
                .LogMatchToConsole(
                    "match: {id: 1 account: 4001 server: 103.40.209.25:443} {id: 2 account: 4003 server: 103.40.209.25:443}");
            loggerMock.Received(1)
                .LogMatchToConsole(
                    "match: {id: 1 account: 4001 server: 103.40.209.25:443} {id: 3 account: 4004 server: 103.40.209.25:443}");
            loggerMock.Received(1)
                .LogMatchToConsole(
                    "match: {id: 3 account: 4004 server: 103.40.209.25:443} {id: 4 account: 4008 server: 103.40.209.25:443}");
            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Where(b => b.Id > 4).Select(x => x.Id).OrderBy(y => y);
            var actual = collection.Select(x => x.Id);
            Assert.True(actual.SequenceEqual(expected));
        }

        [InlineData("1000deals")]
        [InlineData("multiServer")]
        [Theory]
        public void Trigger_1000_CheckOrdering(string fileName)
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\TestData\\{fileName}.json"));


            var loggerMock = new Logger(); ;

            ConfigurationManager.AppSettings["openTimeDelta"] = "5000";
            ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"] = "5";

            var dict = new SingletonDictionary();
            var detector = new Detector(loggerMock, dict);

            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }
            

            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Select(x => x.Id).OrderBy(y => y);
            var actual = collection.Select(x => x.Id);
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void Trigger_1000_MultiSymbol()
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + $"\\TestData\\multiSymbol.json"));


            var loggerMock = new Logger(); ;

            ConfigurationManager.AppSettings["openTimeDelta"] = "5000";
            ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"] = "5";

            var dict = new SingletonDictionary();
            var detector = new Detector(loggerMock, dict);

            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }


            var collection1 = dict.GetDealsBySymbol(data[0].Symbol);
            var collection2 = dict.GetDealsBySymbol(data[199].Symbol);
            var collection3 = dict.GetDealsBySymbol(data[399].Symbol);
            var collection4 = dict.GetDealsBySymbol(data[599].Symbol);
            var collection5 = dict.GetDealsBySymbol(data[799].Symbol);

            Assert.Equal(200, collection1.Length);
            Assert.Equal(200, collection2.Length);
            Assert.Equal(200, collection3.Length);
            Assert.Equal(200, collection4.Length);
            Assert.Equal(200, collection5.Length);
        }

        [Fact]
        public void Same_Deals_Different_Servers()
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(@"[" +
                                                                                 "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000000,\"Counter\":1,\"ServerName\":\"Server1\",\"Login\":4001}," +
                                                                                 "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000001000,\"Counter\":2,\"ServerName\":\"Server2\",\"Login\":4003}," +
                                                                                 "]");

            var loggerMock = Substitute.For<ILogger>();

            ConfigurationManager.AppSettings["openTimeDelta"] = "5000";
            ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"] = "5";

            var dict = new SingletonDictionary();
            var detector = new Detector(loggerMock, dict);


            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }
            loggerMock.Received(1)
                .LogMatchToConsole(
                    "match: {id: 1 account: 4001 server: Server1} {id: 1 account: 4003 server: Server2}");
            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Select(x => x.Id).OrderBy(y => y);
            var actual = collection.Select(x => x.Id);
            Assert.True(actual.SequenceEqual(expected));
        }

        [Fact]
        public void Same_Deals_Same_Servers()
        {
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Deal>>(@"[" +
                                                                                 "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000000000,\"Counter\":1,\"ServerName\":\"Server1\",\"Login\":4001}," +
                                                                                 "{\"Id\":1,\"Symbol\":\"EURUSD\",\"Action\":0,\"VolumeToBalanceRatio\":100,\"Time\":1000000001000,\"Counter\":2,\"ServerName\":\"Server1\",\"Login\":4003}," +
                                                                                 "]");

            var loggerMock = Substitute.For<ILogger>();

            ConfigurationManager.AppSettings["openTimeDelta"] = "5000";
            ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"] = "5";

            var dict = new SingletonDictionary();
            var detector = new Detector(loggerMock, dict);


            for (int i = 0; i < data.Count(); i++)
            {
                dict.AddDeal(data[i]);
            }
            loggerMock.DidNotReceive()
                .LogMatchToConsole(Arg.Any<string>());
            var collection = dict.GetDealsBySymbol(data[0].Symbol);
            var expected = data.Select(x => x.Id).OrderBy(y => y);
            var actual = collection.Select(x => x.Id);
            Assert.True(actual.SequenceEqual(expected));
        }
    }
}
