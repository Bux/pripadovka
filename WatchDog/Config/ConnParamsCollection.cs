﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace WatchDog.Config
{
    public class ConnParamsCollection : ConfigurationElementCollection
    {
        public ConnParam this[int index]
        {
            get => (ConnParam)BaseGet(index);

            set => BaseAdd(index, value);
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ConnParam();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConnParam)element).Name;
        }

        public new IEnumerator<ConnParam> GetEnumerator()
        {
            return Enumerable.Range(0, base.Count).Select(base.BaseGet).Cast<ConnParam>().GetEnumerator();
        }
    }
}
