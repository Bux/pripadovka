﻿
using System.Configuration;

namespace WatchDog.Config
{
    public class ServerSection : ConfigurationSection
    {
        private ServerSection() { }

        [ConfigurationProperty("ConnParams")]
        [ConfigurationCollection(typeof(ConnParamsCollection))]
        public ConnParamsCollection ConnParams
        {
            get { return this["ConnParams"] as ConnParamsCollection; }
            set { this["ConnParams"] = value; }
        }
    }
}
