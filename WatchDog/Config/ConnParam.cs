﻿using System;
using System.Configuration;

namespace WatchDog.Config
{
    public class ConnParam : ConfigurationElement
    {
        [ConfigurationProperty("Ip", IsRequired = true)]
        public string Ip
        {
            get => Convert.ToString(this["Ip"]);
            set => this["Ip"] = value;
        }

        [ConfigurationProperty("Name", IsRequired = true)]
        public string Name
        {
            get => Convert.ToString(this["Name"]);
            set => this["Name"] = value;
        }

        [ConfigurationProperty("Password", IsRequired = true)]
        public string Password
        {
            get => Convert.ToString(this["Password"]);
            set => this["Password"] = value;
        }

        [ConfigurationProperty("ConnectionTimeout", DefaultValue = "30000")]
        public string ConnectionTimeout
        {
            get => Convert.ToString(this["ConnectionTimeout"]);
            set => this["ConnectionTimeout"] = value;
        }

        [ConfigurationProperty("Login", IsRequired = true)]
        public string Login
        {
            get => Convert.ToString(this["Login"]);
            set => this["Login"] = value;
        }
    }
}
