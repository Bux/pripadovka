﻿using MT5Wrapper;
using MT5Wrapper.Interface;
using WatchDog.Logic;
using WatchDog.SingletonCollections;

namespace WatchDog.Factories
{
    public interface IWatchDogHandlerFactory
    {
        IWatchDogHandler Create(IMT5Api mt5, ISingletonDictionary dict, ConnectionParams connParams);
    }
}
