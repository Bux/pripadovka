﻿using WatchDog.Logic;
using WatchDog.SingletonCollections;

namespace WatchDog.Factories
{
    public class DetectorFactory : IDetectorFactory
    {
        public IDetector Create(ILogger logger, ISingletonDictionary dict)
        {
            return new Detector(logger, dict);
        }
    }
}
