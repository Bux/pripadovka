﻿using WatchDog.Logic;
using WatchDog.SingletonCollections;

namespace WatchDog.Factories
{
    public interface IDetectorFactory
    {
        IDetector Create(ILogger logger, ISingletonDictionary dict);
    }
}
