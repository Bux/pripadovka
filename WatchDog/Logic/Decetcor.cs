﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using WatchDog.Model;
using WatchDog.SingletonCollections;

namespace WatchDog.Logic
{
    public sealed class Detector : IDetector, IDisposable
    {

        private readonly ILogger _logger;
        private readonly ISingletonDictionary _dict;
        private readonly long _openTimeDelta;
        private readonly decimal _maxRatio;


        public Detector(ILogger logger, ISingletonDictionary dict)
        {
            _logger = logger;
            _dict = dict;
            _dict.DealAddedToQueueEventHandler += QueueEventHandler;

            if (long.TryParse(ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"], out var percentage) ||
                ConfigurationManager.AppSettings["TradeVolumeToBalanceRatio"] != null)
            {
                _maxRatio = 1 - percentage;
            }
            else
            {
                throw new ArgumentException("wrong value in config");
            }

            if (!long.TryParse(ConfigurationManager.AppSettings["openTimeDelta"], out _openTimeDelta) ||
                ConfigurationManager.AppSettings["openTimeDelta"] == null)
            {
                throw new ArgumentException("wrong value in config");
            }
        }

        public async void QueueEventHandler(Deal deal)
        {
            await ProcessDeal(deal);
        }

        private async Task ProcessDeal(Deal deal)
        {
            var groupedBySymbol = _dict.GetDealsBySymbol(deal.Symbol);
            for (int i = groupedBySymbol.Length - 1; i >= 0; i--)
            {
                if (deal.Id == groupedBySymbol[i].Id &&
                     string.Equals(deal.ServerName, groupedBySymbol[i].ServerName, StringComparison.InvariantCultureIgnoreCase) ||
                    groupedBySymbol[i].Time < deal.Time - _openTimeDelta - 1)
                {
                    continue;
                }

                if (CompareRatios(groupedBySymbol[i].VolumeToBalanceRatio, deal.VolumeToBalanceRatio))
                {
                    await _logger.LogMatchToConsole($"match: {{id: {groupedBySymbol[i].Id} account: {groupedBySymbol[i].Login} server: {groupedBySymbol[i].ServerName}}} {{id: {deal.Id} account: {deal.Login} server: {deal.ServerName}}}");
                }
            }

            _dict.RemoveOldElements(deal, _openTimeDelta);
        }


        private bool CompareRatios(decimal volumeToBalanceRatio1, decimal volumeToBalanceRatio2)
        {
            decimal max;
            decimal min;

            if (volumeToBalanceRatio1 > volumeToBalanceRatio2)
            {
                max = volumeToBalanceRatio1;
                min = volumeToBalanceRatio2;
            }
            else
            {
                min = volumeToBalanceRatio1;
                max = volumeToBalanceRatio2;
            }

            return max != 0 && min / max > _maxRatio;
        }

        public void Dispose()
        {
            _dict.DealAddedToQueueEventHandler -= QueueEventHandler;
        }
    }


}
