﻿using System;
using System.Threading.Tasks;

namespace WatchDog.Logic
{
    public class Logger : ILogger
    {
        public async Task LogMatchToConsole(string text)
        {
            await Console.Out.WriteLineAsync(text);
        }
    }
}
