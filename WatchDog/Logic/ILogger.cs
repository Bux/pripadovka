﻿using System.Threading.Tasks;

namespace WatchDog.Logic
{
    public interface ILogger
    {
        Task LogMatchToConsole(string text);
    }
}