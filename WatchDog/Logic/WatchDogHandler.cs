﻿using MT5Wrapper;
using System;
using MetaQuotes.MT5CommonAPI;
using MT5Wrapper.Interface;
using WatchDog.Model;
using System.Threading.Tasks;
using WatchDog.SingletonCollections;

namespace WatchDog.Logic
{


    public sealed class WatchDogHandler : IWatchDogHandler, IDisposable
    {
        private readonly IMT5Api _mt5Api;
        private readonly ISingletonDictionary _dict;
        private readonly string _name;

        public WatchDogHandler(IMT5Api mt5Api, ISingletonDictionary dict, ConnectionParams connParams)
        {
            _mt5Api = mt5Api;
            _dict = dict;
            _name = connParams.Name;
            Initialize(connParams);
        }

        private void Initialize(ConnectionParams connParams)
        {
            try
            {
                _mt5Api.Connect(connParams);

                _mt5Api.DealEvents.DealAddEventHandler += DealEventHandler;
            }
            catch (MT5Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public async void DealEventHandler(object sender, CIMTDeal deal)
        {
            await AddDeal(deal);
        }

        public async Task AddDeal(CIMTDeal deal)
        {
            var myDeal = new Deal
            {
                Action = deal.Action(),
                VolumeToBalanceRatio = _mt5Api.GetUserBalance(deal.Login()) / deal.Volume(),
                Symbol = deal.Symbol(),
                Time = deal.TimeMsc(),
                Id = deal.Deal(),
                ServerName = _name,
                Login = deal.Login()
            };

            await Task.Run(() => _dict.AddDeal(myDeal));
        }

        public void Dispose()
        {
            _mt5Api.DealEvents.DealAddEventHandler -= DealEventHandler;
        }
    }
}
