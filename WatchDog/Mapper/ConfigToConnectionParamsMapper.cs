﻿using System.Configuration;
using MT5Wrapper;
using WatchDog.Config;

namespace WatchDog.Mapper
{
    public static class ConfigToConnectionParamsMapper
    {
        public static ConnectionParams Map(this ConnParam source)
        {
            if (source == null)
            {
                return null;
            }

            if (!ulong.TryParse(source.Login, out var login))
            {
                throw new ConfigurationErrorsException($"Invalid login in config");
            }

            if (!uint.TryParse(source.ConnectionTimeout, out var connectionTimeout))
            {
                throw new ConfigurationErrorsException($"Invalid connectionTimeout in config");
            }

            return new ConnectionParams
            {
                 Login = login,
                 IP = source.Ip,
                 ConnectionTimeout = connectionTimeout,
                 Name = source.Name,
                 Password = source.Password
            };
        }
    }
}
