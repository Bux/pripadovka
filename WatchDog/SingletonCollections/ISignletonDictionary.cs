﻿using WatchDog.Model;

namespace WatchDog.SingletonCollections
{
    public delegate void DealAddedToQueue(Deal deal);

    public interface ISingletonDictionary
    {
        event DealAddedToQueue DealAddedToQueueEventHandler;

        void RemoveOldElements(Deal deal, long openTimeDelta);

        Deal[] GetDealsBySymbol(string symbol);

        void AddDeal(Deal deal);
    }
}
