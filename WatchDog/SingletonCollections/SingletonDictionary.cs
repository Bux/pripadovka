﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using WatchDog.Model;

namespace WatchDog.SingletonCollections
{
    public sealed class SingletonDictionary : ISingletonDictionary
    {
        public event DealAddedToQueue DealAddedToQueueEventHandler;

        private readonly ConcurrentDictionary<string, ConcurrentQueue<Deal>> _deals;

        public SingletonDictionary()
        {
            _deals = new ConcurrentDictionary<string, ConcurrentQueue<Deal>>();
        }

        public void AddDeal(Deal deal)
        {
            _deals.AddOrUpdate(deal.Symbol,
            new ConcurrentQueue<Deal>(new List<Deal> { deal }),
            (key, value) =>
            {
                value.Enqueue(deal);
                return value;
            });

            DealAddedToQueueEventHandler?.Invoke(deal);
        }

        public void RemoveOldElements(Deal deal, long openTimeDelta)
        {
            while (_deals[deal.Symbol].TryPeek(out var oldestDeal))
            {
                if (oldestDeal.Time < deal.Time - openTimeDelta - 1)
                {
                    if (!_deals[deal.Symbol].TryDequeue(out var removedDeal))
                    {
                        Console.WriteLine("something went terribly wrong");
                    }
                }
                else
                {
                    break;
                }
            }
        }

        public Deal[] GetDealsBySymbol(string symbol)
        {
            return _deals[symbol].ToArray();
        }
    }
}
