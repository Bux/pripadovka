﻿using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using MT5Wrapper;
using MT5Wrapper.Interface;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using WatchDog.Config;
using WatchDog.Factories;
using WatchDog.Logic;
using WatchDog.Mapper;
using WatchDog.SingletonCollections;

namespace WatchDog
{
    public class Program
    {
        private static readonly Container _container;

        static Program()
        {
            _container = new Container();
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            _container.Register<IMT5Api, MT5Api>(Lifestyle.Scoped);

            _container.Register<IWatchDogHandlerFactory, WatchDogHandlerFactory>();
            _container.Register<IDetectorFactory, DetectorFactory>();
            _container.Register<ISingletonDictionary, SingletonDictionary>(Lifestyle.Singleton);
            _container.Register<ILogger, Logger>();
                
            _container.Verify();
        }

        public static async Task Main(string[] args)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                var watchdogFactory = _container.GetInstance<IWatchDogHandlerFactory>();
                var detectorFactory = _container.GetInstance<IDetectorFactory>();


                var serverSection = ConfigurationManager.GetSection("ServerSection") as ServerSection;

                if (serverSection?.ConnParams == null)
                {
                    Console.WriteLine("No server to connect to. Please add servers to app.config");
                }

                await Task.Run(() => detectorFactory.Create(_container.GetInstance<ILogger>(),
                    _container.GetInstance<ISingletonDictionary>()));

                foreach (var serverConfig in serverSection.ConnParams)
                {
                    var connParams = serverConfig.Map();
                    await Task.Run(() => watchdogFactory.Create(_container.GetInstance<IMT5Api>(),
                        _container.GetInstance<ISingletonDictionary>(), connParams));
                }

                Thread.Sleep(Timeout.Infinite);
            }
        }
    }
}
