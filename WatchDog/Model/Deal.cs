﻿namespace WatchDog.Model
{
    public class Deal
    {
        public ulong Id { get; set; }

        public string Symbol { get; set; }

        public uint Action { get; set; }

        public decimal VolumeToBalanceRatio { get; set; }

        public long Time { get; set; }
        
        public string ServerName { get; set; }

        public ulong Login { get; set; }
    }
}
